<?php

/**
 * Visitor Traffic Statistics
 *
 * @package           VisitorTrafficStatistics
 * @author            Al-Amin Ahamed
 * @copyright         2021 Al-Amin Ahamed
 * @license           GPL-3.0-only
 *
 * @wordpress-plugin
 * Plugin Name:       Visitor Traffic Statistics
 * Plugin URI:        https://www.mishusoft.com/wp-plugin/visitor-traffic-statistics/
 * Description:       WP plugin for real time visitor traffic statistics
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Al-Amin Ahamed
 * Author URI:        https://www.mishusoft.com/
 * Text Domain:       vt-statistics
 * License:           GPL-3.0-only
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Update URI:        https://www.mishusoft.com/api/update/wp-plugin/visitor-traffic-statistics/
 */
